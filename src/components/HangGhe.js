import React, { Component } from "react";
import { connect } from "react-redux";
import { datGheAction } from "../redux/actions/BookingTicketActions";

class HangGhe extends Component {
  renderGhe = () => {
    return this.props.hangGhe.danhSachGhe.map((item, index) => {
      let cssGheDaDat = "";
      let disabled = false;
      //trạng thái ghế đã bị đặt
      if (item.daDat) {
        cssGheDaDat = "gheDuocChon";
        disabled = true;
      }
      //xét trạng thái đặt
      let cssGheDangDat = "";
      let indexGheDangDat = this.props.danhSachGheDangDat.findIndex(
        (gheDangDat) => gheDangDat.soGhe === item.soGhe
      );
      if (indexGheDangDat !== -1) {
        cssGheDangDat = "gheDangChon";
      }

      return (
        <button
          disabled={disabled}
          className={`ghe ${cssGheDaDat} ${cssGheDangDat}`}
          key={index}
          onClick={() => {
            this.props.datGhe(item);
          }}
        >
          {item.soGhe}
        </button>
      );
    });
  };

  render() {
    return (
      <div className="text-light text-left ml-5 mt-3">
        {this.props.hangGhe.hang}
        {this.renderGhe()}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.BookingTicketReducer.danhSachGheDangDat,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    datGhe: (item) => {
      dispatch(datGheAction(item));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HangGhe);
