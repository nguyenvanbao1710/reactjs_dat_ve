import { DAT_GHE, HUY_GHE } from "../constants/BookingTicketConstans";

export const datGheAction = (item) => {
  return {
    type: DAT_GHE,
    item,
  };
};

export const huyGheAction = (soGhe) => {
  return {
    type: HUY_GHE,
    soGhe,
  };
};
